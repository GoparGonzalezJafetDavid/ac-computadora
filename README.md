```plantuml
@startmindmap 
title Von-Neumann vs Hardvard
*[#ff0000] Arquitectura
**[#ff5232] Von-Neumann
***[#ff7b5a] Nacio 28 dic 1903 en Budapest hungria \ny Murió 8 feb 1957 Washington DC
****[#ff9e81] 30 julio 1945 desarrollo un programa
*****[#ffbfaa] consistía en el almacenado en memoria de datos
****[#ff9e81] Hizo aportaciones al diseño lógico y de programación
****[#ff9e81] Propuso el bit como medida de memoria
****[#ff9e81] Resolvió el problema de la obtención de respuestas fiables
*****[#ffbfaa] con componentes no fiables(bit de paridad)
****[#ff9e81] Participo en ll proyecto ENIAC
*****[#ffbfaa] Primer computador de propósito general.
****[#ff9e81] Propuso separar el hardware del software 
****[#ff9e81] Su arquitectura consiste en bloques funcionales que están conectados entre si
*****[#ffbfaa] Se compone de 4 elementos básicos 
******[#ffdfd4] Procesador
******[#ffdfd4] Memoria
******[#ffdfd4] Dispositivos de entrada y salida
******[#ffdfd4] Medio de conexion (Buses)
**[#ff5232] Hardvard
***[#ff7b5a] Nacio el 8 de marzo de 1900, Hoboken, Nueva Jersey \ny murio un 14 de marzo de 1973, San Luis, Misuri
****[#ff9e81] Diseño un dispositivo electromecánico 
****[#ff9e81] Ing principal tras el proyecto de ordenadores Mark 
*****[#ffbfaa] Fue el primer ordenador electromecánico
****[#ff9e81] La arquitectura Harvard 
*****[#ffbfaa] Empleaba unidades de memoria separadas 
******[#ffdfd4] Para almacenar datos e instrucciones con buses separados
@endmindmap
```

```plantuml
@startmindmap 
title   Supercomputadoras en México
*[#444b65] Historia de la \ncomputación en mexico 
**[#5a6488] La UNAM
***[#717fae] Obtuvo la primera gran computadora\n en latinoamerica
****[#899ad5] La cual fue la IBM-650 
*****[#b1bae3] Construida de bulbos y tarjetas perforadas

***[#717fae] KANBALAM
****[#899ad5] Reduce calculos de 25 años a 1 semana
****[#899ad5] Desarrola investigaciones en astronomia, \nQuimica cuantica etc.
****[#899ad5] 7 billones de operaciones por segundo
****[#899ad5] Actual computadora de la UNAM

***[#717fae] MIZTLI
****[#899ad5] 228 teraflops de velocidad en operaciones \nde punto flotante en 1 segundo
****[#899ad5] Estudia la estructura del universo, sismos, \n y comportamiento de particulas \nsubatomicas
****[#899ad5] Asi como farmacos y reactores nucleares.

***[#717fae] XIUCOALT
****[#899ad5] En el año 2012
*****[#b1bae3] se creo xiuhcoatl 
******[#b1bae3] Por las universidades de \nUNAM, UAM, IPN
****[#899ad5] Caracteristicas principales
*****[#b1bae3] Nodos robustos de supercomputo \nen los centros de datos de las universidades
*****[#b1bae3] Delta metropolitana de supercomputo red de fibra optica

***[#717fae] BUAP
****[#899ad5] Desarrolla supercomputadora
*****[#b1bae3] 1 de las 5 mas poderosas en latinoamerica
*****[#b1bae3] Super capacidade de almacenamiento
*****[#b1bae3] 2 mil millones de operaciones en 1 segundo
*****[#b1bae3] Capaz de crear grandes simulaciones
@endmindmap
```
